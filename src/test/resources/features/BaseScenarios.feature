@IphoneX
Feature: BaseScenarios
  These scenarios can be used in any project

  Background:
    Given the Artistry app has been launched

  Scenario: 01. Validate the PageSource string on the app screen
    Then I see "Vincent Willem van Gogh" in the PageSource

  Scenario: 02. Andy Warhol has info on his Elvis painting
    When I choose to view paintings by Andy Warhol
    And I decide to read more info on the Eight Elvises painting
    Then I see "1963 silkscreen painting by American pop artist" in the PageSource