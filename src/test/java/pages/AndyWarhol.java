package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.*;

import static utils.extensions.XCTestExtensions.*;

public class AndyWarhol extends BasePage {

    public AndyWarhol(IOSDriver driver) {
        super(driver);
    }

    @iOSXCUITFindBy(accessibility = "Eight Elvises")
    public IOSElement btnEightElvises;

    public void viewMoreInfoOnEightElvises() {
        xcScroll("down", "predicateString", "(XCUIElementTypeCell)[2]");
        btnEightElvises.click();
    }
}
