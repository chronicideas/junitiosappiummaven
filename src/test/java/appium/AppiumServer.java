package appium;

import io.appium.java_client.service.local.AppiumDriverLocalService;

public class AppiumServer {

    private static AppiumDriverLocalService service = AppiumDriverLocalService.buildDefaultService();

    public static void start() {
        service.start();
    }

    public static void stop() {
        service.stop();
    }
}
