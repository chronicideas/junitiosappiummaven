package appium;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class CreateDriver {

    private CreateDriver() {

    }

    private static CreateDriver instance = null;

    private static ThreadLocal<IOSDriver<IOSElement>> iosDriver = new ThreadLocal<>();

    public static CreateDriver getInstance() {
        if (instance == null) {
            instance = new CreateDriver();
        }
        return instance;
    }

    public void setDriver(String platformVersion, String deviceName) throws MalformedURLException {
        AppiumServer.start();

        File file = new File("src");
        File app = new File(file + "/Artistry.app");

        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("automationName", "XCUITest");
        cap.setCapability("platformVersion", platformVersion);
        cap.setCapability("deviceName", deviceName);
        cap.setCapability("app", app.getAbsolutePath());

        iosDriver.set(new IOSDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap));
    }

    public IOSDriver<IOSElement> getDriver() {
        return iosDriver.get();
    }

    public synchronized void tearDown() {
        iosDriver.get().quit();
        AppiumServer.stop();
    }
}
