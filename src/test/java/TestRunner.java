import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"hooks", "steps"},
        tags = {"~@Ignore"}
)

@RunWith(Cucumber.class)
public class TestRunner {

}
