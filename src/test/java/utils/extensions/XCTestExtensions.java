package utils.extensions;

import appium.CreateDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

import java.util.HashMap;
import java.util.Map;

public class XCTestExtensions {

    private static IOSDriver driver = CreateDriver.getInstance().getDriver();

    //Swipe gestures
    public static void xcSwipe(String direction) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        driver.executeScript("mobile: swipe", params);
    }

    public static void xcSwipe(String direction, int j) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        for (int i = 0; i < j; i++) {
            driver.executeScript("mobile: swipe", params);
        }
    }

    public static void xcSwipe(String direction, IOSElement element) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        params.put("element", element.getId());
        driver.executeScript("mobile: swipe", params);
    }

    //Scroll gestures
    public static void xcScroll(String direction) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        driver.executeScript("mobile: scroll", params);
    }

    public static void xcScroll(String direction, int numOfScrolls) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        for (int i = 0; i < numOfScrolls; i++) {
            driver.executeScript("mobile: scroll", params);
        }
    }

    public static void xcScroll(String direction, String nameOrPredicateStringParam, String nameOrPredicateString) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        params.put(nameOrPredicateStringParam, nameOrPredicateString);
        driver.executeScript("mobile: scroll", params);
    }

    public static  void xcScroll(String direction, IOSElement element, boolean toVisible) {
        Map<String, Object> params = new HashMap<>();
        params.put("direction", direction);
        params.put("element", element);
        params.put("toVisible", toVisible);
        driver.executeScript("mobile: swipe", params);
    }
}