package hooks;

import appium.CreateDriver;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.net.MalformedURLException;

public class CucumberHooks {

    @Before("@Iphone8")
    public void beforeIphone8() throws MalformedURLException {
        CreateDriver.getInstance().setDriver("12.1", "iPhone 8");
    }

    @Before("@IphoneX")
    public void beforeIphoneX() throws MalformedURLException {
        CreateDriver.getInstance().setDriver("12.1", "iPhone X");
    }

    @After
    public void after() {
        CreateDriver.getInstance().tearDown();
    }
}
